FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build

RUN apt-get update -yq && apt-get upgrade -yq && apt-get install -yq curl git nano
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -yq nodejs build-essential

RUN npm install -g npm

RUN npm install -g vue-cli 

WORKDIR /src
